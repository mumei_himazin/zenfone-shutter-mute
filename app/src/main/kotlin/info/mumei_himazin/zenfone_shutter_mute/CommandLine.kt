package info.mumei_himazin.zenfone_shutter_mute

import android.util.Log
import java.io.*

class CommandLine{

    private val process:Process
    private val output: OutputStream
    private val input: InputStream
    private val errorInput:InputStream

    constructor(su:Boolean = false){
        process = Runtime.getRuntime().exec(if(su)"su" else "sh")
        output = process.outputStream
        input = process.inputStream
        errorInput = process.errorStream

        if(su){
            if(write("su -v\n")){
                Log.d("test",read(true))
            }

        }
    }

    fun write(command:String):Boolean{
        return try {
            output.write(command.toByteArray(Charsets.UTF_8))
            output.flush()
            true
        }catch(e: IOException){
            false
        }
    }

    fun read(block:Boolean=false,timeout:Int=2000):String{
        var data = ByteArray(1024)
        val builder = StringBuilder()
        if(block&&input.available()<=0){
            val time = System.currentTimeMillis()
            while(input.available()<=0){
                if(System.currentTimeMillis()-time>timeout)break
                Thread.sleep(100)
            }
        }
        while(input.available()>0){
            val size = input.read(data)
            if(size>0)builder.append( String(data,0,size,Charsets.UTF_8) )
        }
        return builder.toString()
    }

    fun close(){
        try {
            input.close()
        }catch(e:Throwable){}
        try {
            output.close()
        }catch(e:Throwable){}
        try {
            process.destroy()
        }catch(e:Throwable){}
    }
}
