package info.mumei_himazin.zenfone_shutter_mute;

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileOutputStream
import kotlin.concurrent.withLock

class MainActivity : AppCompatActivity() {

    var busyBoxPath:String? = null
    var command:CommandLine? = null


     override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        command = CommandLine(true)

         busyBoxPath = initBusybox()

         enableFocusSound.setOnClickListener {
            enableFocusSound(true)
             Toast.makeText(this,"enable focus sound",Toast.LENGTH_LONG).show()
        }

         disableFocusSound.setOnClickListener {
             enableFocusSound(false)
             Toast.makeText(this,"disable focus sound",Toast.LENGTH_LONG).show()
         }

         enableShutterSound.setOnClickListener {
             enableShutterSound(true)
             Toast.makeText(this,"enable shutter sound",Toast.LENGTH_LONG).show()
         }

         disableShutterSound.setOnClickListener {
             enableShutterSound(false)
             Toast.makeText(this,"disable shutter sound",Toast.LENGTH_LONG).show()
         }

         reboot.setOnClickListener {
             command?.write("reboot\n")
         }
    }

    override fun onDestroy() {
        super.onDestroy()
        command?.close()
    }

    fun enableShutterSound(enable:Boolean){
        val lock = java.util.concurrent.locks.ReentrantLock()
        lock.withLock {
            command?.write("mount -o rw,remount -t ext4 /dev/block/by-name/system /system\n")
            try {
                val c = if(enable){
                    "rename /system/media/audio/ui/camera_click.ogg.bak /system/media/audio/ui/camera_click.ogg\n"
                }else{
                    "rename /system/media/audio/ui/camera_click.ogg /system/media/audio/ui/camera_click.ogg.bak\n"
                }
                Log.d("c",c)
                command?.write(c)
            }finally{
                command?.write("mount -o ro,remount -t ext4 /dev/block/by-name/system /system\n")
            }
        }
    }


    fun enableFocusSound(enable:Boolean){
        val lock = java.util.concurrent.locks.ReentrantLock()
        lock.withLock {
            command?.write("mount -o rw,remount -t ext4 /dev/block/by-name/system /system\n")
            try {
                if (command?.write("ls /system|grep .*\\.prop\n") ?: false) {
                    command?.read(true, 100)?.split("\n")?.forEach {
                        if(it.isBlank())return@forEach
                        var target = "/system/$it"
                        var c = if(enable){
                            "$busyBoxPath sed -i -e \"/^ro.camera.sound.forced/s/0/1/\" $target\n"
                        }else{
                            "$busyBoxPath sed -i -e \"/^ro.camera.sound.forced/s/1/0/\" $target\n"
                        }
                        Log.d("c", c)
                        command?.write(c)
                        Log.d("result", command?.read(true,100) )
                    }
                }
            }finally{
                command?.write("mount -o ro,remount -t ext4 /dev/block/by-name/system /system\n")
            }
        }
    }

    fun initBusybox():String{
        val busyboxFile = File(getFilesDir(),"busybox")
        if(!busyboxFile.exists()) {
            val out = FileOutputStream(busyboxFile)
            val input = assets.open("busybox")
            input.copyTo(out);
            input.close()
            out.close()
        }
        command?.write("chmod 777 ${busyboxFile.path}\n")
        return busyboxFile.path


    }
}


/*
mount -o ro,remount -t ext4 /dev/block/by-name/system /system
mount -o rw,remount -t ext4 /dev/block/by-name/system /system
 */